class CreateHotelData < ActiveRecord::Migration
  def change
    create_table :hotel_data do |t|

      t.timestamps null: false

      t.integer :hotel_id
      t.integer :hotel_api
      t.integer :stars
      t.boolean :stop_sale
      t.boolean :confirmation_express
      t.text    :child_policy_description
      t.datetime :solicited_at
      
    end
  end
end




