class CreateGenericData < ActiveRecord::Migration
  def change
    create_table :generic_data do |t|
      t.text :content

      t.timestamps
    end
  end
end
