class CreateAerialData < ActiveRecord::Migration
  def change
    create_table :aerial_data do |t|
      t.string :pnr      
      t.timestamps
    end
  end
end
