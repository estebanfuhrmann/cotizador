class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|

    	t.decimal :value
      t.string  :category
      t.string  :base
      t.string  :regime      
      t.text    :add_regime
    	t.text    :adults
      t.text    :childs
      t.text    :minors

      t.references :tariff, index: true
      t.references :hotel_data, index: true
      t.timestamps null: false
    end
  end
end