class CreateExcursionData < ActiveRecord::Migration
  def change
    create_table :excursion_data do |t|
      t.string :excursion_type
      t.string :pick_up

      t.timestamps
    end
  end
end
