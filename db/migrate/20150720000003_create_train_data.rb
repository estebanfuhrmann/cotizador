class CreateTrainData < ActiveRecord::Migration
  def change
    create_table :train_data do |t|
      t.string :from
      t.string :to
      t.time :departure_time

      t.timestamps
    end
  end
end
