class CreateTariff < ActiveRecord::Migration
  def change
    create_table :tariffs do |t|

    	t.timestamps null: false
    	t.decimal  :markup
    	t.boolean  :markup_is_fixed
    	t.string   :currency
    	t.text     :taxes

    end
  end
end
