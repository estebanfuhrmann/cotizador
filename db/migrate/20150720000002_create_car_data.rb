class CreateCarData < ActiveRecord::Migration
  def change
    create_table :car_data do |t|
      t.string :pnr
      t.string :car_type
      t.string :plan
      t.string :pick_up
      t.string :pick_up_time
      t.string :drop_off
      t.string :drop_off_time

      t.timestamps
    end
  end
end
