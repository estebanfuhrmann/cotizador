class CreateCruiseData < ActiveRecord::Migration
  def change
    create_table :cruise_data do |t|
      t.timestamps
    end
  end
end
