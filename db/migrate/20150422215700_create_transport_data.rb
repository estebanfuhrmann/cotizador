class CreateTransportData < ActiveRecord::Migration
  def change
    create_table :transport_data do |t|
      t.timestamps
      t.text :transfers
    end
  end
end
