class CreateCircuitData < ActiveRecord::Migration
  def change
    create_table :circuit_data do |t|
      t.string :category
      t.string :base
      t.string :regime

      t.timestamps
    end
  end
end
