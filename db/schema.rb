# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150720000008) do

  create_table "aerial_data", force: :cascade do |t|
    t.string   "pnr",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assistance_data", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_data", force: :cascade do |t|
    t.string   "pnr",           limit: 255
    t.string   "car_type",      limit: 255
    t.string   "plan",          limit: 255
    t.string   "pick_up",       limit: 255
    t.string   "pick_up_time",  limit: 255
    t.string   "drop_off",      limit: 255
    t.string   "drop_off_time", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "circuit_data", force: :cascade do |t|
    t.string   "category",   limit: 255
    t.string   "base",       limit: 255
    t.string   "regime",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cruise_data", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "excursion_data", force: :cascade do |t|
    t.string   "excursion_type", limit: 255
    t.string   "pick_up",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "generic_data", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_data", force: :cascade do |t|
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "hotel_id",                 limit: 4
    t.integer  "hotel_api",                limit: 4
    t.integer  "stars",                    limit: 4
    t.boolean  "stop_sale",                limit: 1
    t.boolean  "confirmation_express",     limit: 1
    t.text     "child_policy_description", limit: 65535
    t.datetime "solicited_at"
  end

  create_table "products", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "data_id",    limit: 4
    t.string   "data_type",  limit: 255
    t.string   "name",       limit: 255
    t.date     "date_in"
    t.date     "date_out"
    t.integer  "priority",   limit: 4
    t.integer  "api",        limit: 4
    t.integer  "tariff_id",  limit: 4
  end

  add_index "products", ["data_type", "data_id"], name: "index_products_on_data_type_and_data_id", using: :btree
  add_index "products", ["tariff_id"], name: "index_products_on_tariff_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.decimal  "value",                       precision: 10
    t.string   "category",      limit: 255
    t.string   "base",          limit: 255
    t.string   "regime",        limit: 255
    t.text     "add_regime",    limit: 65535
    t.text     "adults",        limit: 65535
    t.text     "childs",        limit: 65535
    t.text     "minors",        limit: 65535
    t.integer  "tariff_id",     limit: 4
    t.integer  "hotel_data_id", limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "rooms", ["hotel_data_id"], name: "index_rooms_on_hotel_data_id", using: :btree
  add_index "rooms", ["tariff_id"], name: "index_rooms_on_tariff_id", using: :btree

  create_table "service_data", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tariffs", force: :cascade do |t|
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.decimal  "markup",                        precision: 10
    t.boolean  "markup_is_fixed", limit: 1
    t.string   "currency",        limit: 255
    t.text     "taxes",           limit: 65535
  end

  create_table "train_data", force: :cascade do |t|
    t.string   "from",           limit: 255
    t.string   "to",             limit: 255
    t.time     "departure_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transport_data", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "transfers",  limit: 65535
  end

end
