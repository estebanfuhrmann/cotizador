class CircuitData < ActiveRecord::Base
  has_one  :product, as: :data
end