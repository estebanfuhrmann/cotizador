class HotelData < ActiveRecord::Base
  has_one  :product, as: :data
  has_many :rooms, dependent: :destroy

  accepts_nested_attributes_for :rooms, reject_if: :all_blank

end