class Product < ActiveRecord::Base

	DATA_TYPES = ['AerialData', 'AssistanceData', 'CarData', 'CircuitData', 'CruiseData', 'ExcursionData', 'HotelData', 'TrainData', 'TransportData', 'GenericData']

	belongs_to :data, polymorphic: true, dependent: :destroy
	belongs_to :booking
	belongs_to :tariff, dependent: :destroy
	
	accepts_nested_attributes_for :tariff, reject_if: :all_blank
	accepts_nested_attributes_for :data


	def build_data(params) # Needed for nested-forms
	  raise "Unknown data_type: #{data_type}" unless DATA_TYPES.include?(data_type)
	  self.data = data_type.constantize.new(params)
	end

end
