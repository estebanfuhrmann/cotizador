class ServiceData < ActiveRecord::Base
  has_one :product, as: :data
end
