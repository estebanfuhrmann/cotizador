module Geography
require 'active_resource'
  
  class AeroGeographyProvider < ActiveResource::Base
    self.site = ENV["GEOGRAPHY"]
    self.timeout = 2
  end

  class Region < AeroGeographyProvider
  end

  class Country < AeroGeographyProvider
  end

  class Area < AeroGeographyProvider
  end

  class Destination < AeroGeographyProvider
  end

end