require "geography"
class SearchesController < ApplicationController
  before_action :set_search, only: [:query_search]

  def search
    @search      = Search.new
    @searchHotel = SearchHotel.new
    @searchHotel.rooms = 4.times.map { SearchRoom.new }
  end

  def hotel_search
    @searchHotel = SearchHotel.new(params[:search_hotel])
    @results = @searchHotel.call
    render 'search_result'
  end


  private
  
    def set_search
      @search = Search.new(search_params)
    end

    def search_params
      params.permit :destination_id, :date_in, :date_out, :nights, :childs, :minors, :adults
    end

    def hotel_search_params
      params.permit :destination_id, :date_in, :date_out
    end

end
