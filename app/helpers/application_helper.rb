module ApplicationHelper
  def fa_link_to(text, url, options = {})
    icon = options.delete :icon
    text = content_tag(:i, '', :class => "fa fa-#{icon}") << " " << text  unless icon.blank?
    link_to text, url, options
  end	
end
