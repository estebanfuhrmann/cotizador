class Room < ActiveRecord::Base
  BASES   = [:single, :double, :triple, :quadruple, :quintuple, :sextuple]
  REGIMES = ['DES', 'EP', 'MAP', 'AI', 'PC']

  belongs_to    :tariff, dependent: :destroy
  accepts_nested_attributes_for :tariff, reject_if: :all_blank
end


