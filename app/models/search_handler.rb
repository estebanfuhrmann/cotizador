class SearchHandler

	attr_accessor :hotels

	def self.call(search)
		self.send(search.data_type, search)
	end

	private
		
		def self.hotel_data(search)
			#Me conecto a la app
			#Armo el conjunto de productos
			#retorno los productos
			results = JSON.parse(File.read("test/fixtures/hotels.json"))
			results["hotels"].collect{|product_json| Product.new(product_json)}
		end

end
