class SearchHotel < Search
  attr_accessor :hotel_id, :cant_rooms, :rooms
  attr_reader   :data_type

  def initialize(attributes = nil)
    @data_type = "hotel_data"
    super
  end

  def rooms_attributes=(attributes)
    @rooms ||= []
    attributes.each do |i, room_params|
      @rooms.push(SearchRoom.new(room_params))
    end
  end

  def call
    SearchHandler.call(self)
  end

end