class SearchRoom 
	include ActiveModel::Model
	
  BASES   = [:single, :double, :triple, :quadruple, :quintuple, :sextuple]
  REGIMES = ['DES', 'EP', 'MAP', 'AI', 'PC']

  attr_accessor :category , :base, :regime, :child, :adult, :minor

end
